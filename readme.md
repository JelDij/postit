# PostIT

This is a rather straightforward web page in which the user can create, view and delete post its. The functionality is rather basic, but the point of this project was to familiarize myself with Stencil.js.

Note that postIts are only saved in the application, so a refresh of your page will delete the postIts. Again, it is meant as an exercise.

## Running the app

To run the application, clone the repository. Once cloned, navigate to the directory and run:

```bash
npm install
```

Next run: 

```bash
npm start
```

The application will automatically open in a new web browser window.
