import {PostIt} from "../model/PostIt";

class PostItServiceController {
  public myPostIts: PostIt[];
  private counter: number;

  constructor() {
    this.myPostIts = [];
  }

  getPostIts() {
    return this.myPostIts;
  }

  addPostIt(postIt: PostIt) {
    postIt.id = this.counter;
    this.myPostIts.push(postIt);
    this.counter++;
  }

  deletePostIt(postIt: PostIt) {
    this.myPostIts = this.myPostIts.filter((value: PostIt) => { return value !== postIt});
  }
}
  export const PostItService = new PostItServiceController();
