export interface PostIt {
  id?: number;
  title: string;
  content: string;
  date: Date;
}
