import {Component, h, Prop, State} from '@stencil/core';
import { RouterHistory} from "@stencil/router";

import {PostIt} from "../../model/PostIt";
import { PostItService } from "../../service/PostItService";


@Component({
  tag: 'new-postit',
  styleUrl: 'new-postit.scss',
  shadow: true,
})
export class NewPostit {

  @State() title: string = "Title of your PostIT";
  @State() content: string = "Type something!"
  @Prop() history: RouterHistory;

  handleSubmit(e) {
    e.preventDefault();
    const newPostIt: PostIt = {
      title: this.title,
      content: this.content,
      date: new Date(),
    }
    PostItService.addPostIt(newPostIt);
    this.history.push('/', {});
  }

  handleTitleChange(event) {
    this.title = event.target.value;
  }

  handleContentChange(event) {
    this.content = event.target.value;
  }

  render() {
    return (
      <form onSubmit={(e) => this.handleSubmit(e)}>
      <div class="card">
        <div class="card-body">
          <h5 class="card-title">
            <input type='text' value={this.title} onInput={(event) => this.handleTitleChange(event)}></input>
          </h5>
          <p class="card-text">
            <textarea cols={30} value={this.content} onInput={(event) => this.handleContentChange(event)}></textarea>
          </p>
          <input class="submit" type="submit" value="Save"/>
        </div>
      </div>
      </form>
    );
  }

}
