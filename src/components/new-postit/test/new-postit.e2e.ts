import { newE2EPage } from '@stencil/core/testing';

describe('new-postit', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<new-postit></new-postit>');

    const element = await page.find('new-postit');
    expect(element).toHaveClass('hydrated');
  });
});
