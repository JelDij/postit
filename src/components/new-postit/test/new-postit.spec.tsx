import { newSpecPage } from '@stencil/core/testing';
import { NewPostit } from '../new-postit';

describe('new-postit', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [NewPostit],
      html: `<new-postit></new-postit>`,
    });
    expect(page.root).toEqualHtml(`
      <new-postit>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </new-postit>
    `);
  });
});
