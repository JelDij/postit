import {Component, h} from '@stencil/core';
import { PostItService} from "../../service/PostItService";
import {PostIt} from "../../model/PostIt";

@Component({
  tag: 'app-home',
  styleUrl: 'app-home.css',
  shadow: true,
})
export class AppHome {

  private postItList: PostIt[];

  render() {
    this.postItList = PostItService.getPostIts();
    return (
      <div class="app-home">
        <p>
          Welcome to Pencil PostIT. The place to store your thoughts, todo's, whatever you want. Click anywhere to add a postIT.
        </p>
        <stencil-route-link url="/new-postit/">
          <button>Create new PostIT</button>
        </stencil-route-link>
        <post-it-list
          postItList={this.postItList}></post-it-list>
      </div>
    );
  }

}
