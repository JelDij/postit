import {Component, Host, h, Prop} from '@stencil/core';
import {PostIt} from "../../model/PostIt";
import { PostItService} from "../../service/PostItService";

@Component({
  tag: 'post-it-list',
  styleUrl: 'post-it-list.scss',
  shadow: true,
})
export class PostItList {

  @Prop() postItList: PostIt[];

  deletePostIt(postIt: PostIt) {
    PostItService.deletePostIt(postIt);
    this.postItList = PostItService.getPostIts();
  }

  render() {
    return (
      <Host>
        <slot>
          <div>
            {this.postItList.map((postIt: PostIt) =>
              <div class="card">
                <div class="card-body">
                  <h5 class={"card-title"}>{postIt.title}</h5>
                  <p class="card-text">{postIt.content}</p>
                  <p class="card-text">{postIt.date.toLocaleDateString()}</p>
                  <button class="btn-light" onClick={() => this.deletePostIt(postIt)}>Delete</button>
                </div>
              </div>
            )}
          </div>
        </slot>
      </Host>
    );
  }

}
