import { newSpecPage } from '@stencil/core/testing';
import { PostItList } from '../post-it-list';

describe('post-it-list', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [PostItList],
      html: `<post-it-list></post-it-list>`,
    });
    expect(page.root).toEqualHtml(`
      <post-it-list>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </post-it-list>
    `);
  });
});
