import { newE2EPage } from '@stencil/core/testing';

describe('post-it-list', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<post-it-list></post-it-list>');

    const element = await page.find('post-it-list');
    expect(element).toHaveClass('hydrated');
  });
});
